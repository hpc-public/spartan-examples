GEOS (Geometry Engine - Open Source) is a C++ port of the Java Topology Suite (JTS).

Two simple examples are given here from the C API.

The first is the simplest GEOS C API application. It includes the API header, declare a message handler, initialises the 
GEOS globals, and links to the GEOS C library when built.

The second is an example of reentrant/threadsafe C API. GEOS functions provide reentrant variants, indicated by an _r 
suffix. 

The reentrant functions work the same as their regular counterparts, but they have an extra parameter, a 
GEOSContextHandle_t.

The GEOSContextHandle_t carries a thread-local state that is equivalent to the state initialized by the initGEOS() call in 
the simple example above.

To use the reentrant API, call GEOS_init_r() instead of initGEOS() to create a context local to your thread. Each thread 
that will be running GEOS operations should create its own context prior to working with the GEOS API.

In this example the overall structure of the code is identical, but the reentrant variants are used, and the preamble and 
cleanup are slightly different.
