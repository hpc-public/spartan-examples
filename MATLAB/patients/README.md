## Mathematical Applications and Programming

The following is derived from the Software Carpentries short session on Octave Programming, but modified for the Spartan HPC environment and 
Slurm job submission (https://bagustris.github.io/octave-tutorial/01-intro/).

Why is it in the MATLAB directory? Because (a) Octave is highly compatiable with MATLAB and this illustrates this and (b) Octave on Spartan 
does not do graphical output and this example makes extensive use of plots etc.

All these tasks are presented in a manner that can be carried out in an interactive session and with an job submission script.

The example dataset is a study of inflammation in patients who have been given a new treatment for arthritis. The data sets are stored in a 
comma-separated value (CSV) format, with each row holding information for a single patient, and each columns represent successive days of the 
study.

Collect example, start interactive session

```
cd ~
cp -r /usr/local/common/MATLAB .
sinteractive --time=4:0:0 --partition="physical"
cd MATLAB/patients/
```

Load modules, start octave

```
module load matlab/2020a
```

Interactive session

Read in the csv file as csv and assign to the variable `patient_data`

`patient_data = csvread('inflammation-01.csv');`

Determine the size of the CSV file, in rows and columns
`size(patient_data)`

Determine the datatype of the contents
`class(patient_data)`

Average inflammation for all patients on all days
`mean(patient_data(:))`

# NOTE!
# The command mean(patient_data) would compute the mean of each column in our table, and return an array of mean values. 
# The expression mean(patient_data(:)) flattens the table into a one-dimensional array.
# The `:` selects all elements along that dimension. So, the index (5, :) selects the elements on row 5, and all 
# columns—effectively, the entire row. 

# Compute other statistics, like the maximum, minimum and standard deviation.
disp(['Maximum inflammation: ', num2str(max(patient_data(:)))]);
disp(['Minimum inflammation: ', num2str(min(patient_data(:)))]);
disp(['Standard deviation: ', num2str(std(patient_data(:)))]);

# When analyzing data, we often want to look at partial statistics, such as the maximum value per patient or the average value per day. One 
# way to do this is to assign the data we want to a new temporary array, then ask it to do the calculation:

patient_1 = patient_data(1, :)
disp(['Maximum inflation for patient 1: ', num2str(max(patient_1))]);

# We don’t actually need to store the row in a variable of its own. Instead, we can combine the selection and the function call:

max(patient_data(1, :))

# What if we need the maximum inflammation for all patients, or the average for each day?
# To support this, Octave allows us to specify the dimension we want to work on. 
# If we ask for the average across the dimension 1, we get:

mean(patient_data, 1)

# If we average across axis 2, we get the average inflammation per patient across all days.

mean(patient_data, 2)


# Turn off image visibility. We're on the cluser!
# This can be removed for FastX interactive jobs

figure('visible', 'off')

# A heat map of the data. The imagesc function represents the matrix as a color image. 
# Every value in the matrix is mapped to a color. Blue regions in this heat map are low values, while red shows high values.
imagesc(patient_data)

# Let's take a look at the average inflammation over time:

ave_inflammation = mean(patient_data, 1);
plot(ave_inflammation);

# The maximum and minimum inflammation per day across all patients.
plot(max(patient_data, [], 1));
title('Maximum inflammation per day');
plot(min(patient_data, [], 1));
title('Minimum inflammation per day');

# It's common to put multiple figures "side-by-side" in a single window for presentation and convenience. 
# Here's how to use the subplot function to do this:

subplot(1, 2, 1);
plot(max(patient_data, [], 1));
ylabel('max')

subplot(1, 2, 2);
plot(min(patient_data, [], 1));
ylabel('min')

