# Example from: https://www.geeksforgeeks.org/python-how-to-search-for-a-string-in-text-files/
# Text from https://randomtextgenerator.com/

string1 = 'portal'
  
# opening a text file
file1 = open("random.txt", "r")
  
# read file content
readfile = file1.read()
  
# checking condition for string found or not
if string1 in readfile: 
    print('String', string1, 'Found In File')
else: 
    print('String', string1 , 'Not Found') 
  
# closing a file
file1.close() 
