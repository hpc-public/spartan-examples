#!/bin/bash
#SBATCH --cpus-per-task=4

module purge
module load python/3.7.4

srun python3 threadsubclass.py
