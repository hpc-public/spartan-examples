#! /usr/bin/env python3

from memory_profiler import profile

@profile
def main_func():
	list_of_numbers = []
	for i in range(1000000):
  		list_of_numbers.append(i)

if __name__ == "__main__":
    main_func()
