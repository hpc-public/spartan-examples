#! /usr/bin/env python3
# From: https://pastebin.com/jp5WXvkH
import subprocess

def mainFunction ():
	text = run (["curl", "https://gitlab.com/es20490446e/express-repository"])

	for index in range (0,10000):
		start = (text.find("script"))
		start = (text.find("\"", start)+1)
		end = (text.find("\"", start))
		#print ("{:1}: {:2}".format(index,text[start:end]))

def run (command):
	output = subprocess.run(
		command,
		stdout = subprocess.PIPE,
		stderr = subprocess.PIPE)

	if output.returncode != 0:
		raise RuntimeError(
			output.stderr.decode("utf-8"))

	return output.stdout.decode("utf-8")


mainFunction ()
