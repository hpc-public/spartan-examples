#! /bin/bash
# From https://pastebin.com/gTpQpUCd

text="$(curl --silent "https://gitlab.com/es20490446e/express-repository")"

for number in {1..10000}; do
	echo "${text}" |
	grep "script" |
	head -n1 |
	cut --delimiter='"' --fields=2 > /dev/null
done
