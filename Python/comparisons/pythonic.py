from time import perf_counter_ns

def main():    
    # non pythonic
    start=perf_counter_ns()
    total=0 
    for i in range(1,100):
        if (i %3)== 0:
            total += i
    end=perf_counter_ns()      
    print (f"Non-Pythonic Total of divisible by 3= {total}")
    print(f"Time took {end-start}")

    # pythonic
    start=perf_counter_ns()
    total =sum(range(1, 100, 3))
    end=perf_counter_ns()      
    print (f"Pythonic Total of divisible by 3= {total}")
    print(f"Time took {end-start}")

if __name__ == "__main__":
    main()
