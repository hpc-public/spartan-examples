#!/usr/bin/env python3

import numpy as np
from memory_profiler import profile

@profile
def main_func():
	arr = np.zeros((1000000,), dtype=np.uint64)
	for i in range(1000000):
		arr[i] = i

if __name__ == "__main__":
    main_func()
