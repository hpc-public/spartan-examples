from time import perf_counter_ns
from functools import lru_cache

def main():    
    def fib(n):
        return n if n < 2 else fib(n-1) + fib(n-2)

    @lru_cache(maxsize=None)
    def mfib(n):
        return n if n < 2 else mfib(n-1) + mfib(n-2)
    start=perf_counter_ns()
    print(f"Non-memoized fib()={fib(35)}")
    end=perf_counter_ns()      
    print(f"Time took {end-start}")

    start=perf_counter_ns()  
    print(f"Memoized fib()={mfib(35)}")
    end=perf_counter_ns()      
    print(f"Time took {end-start}")

if __name__ == "__main__":
    main()
