Derived from content from the University of Agricultural Sciences, Sweden.

Modern sequencing technologies tends to produce reads that have deteriorating quality towards the 3'-end and/or 5'-end.

The reads in the dataset are trimmed to keep the good quality part uisng sickle.

The dataset is created from the output form an example job using Scythe (q.v).



