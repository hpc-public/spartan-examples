# Gretl 

This introduction is largely derived from "A Hansl Primer", Allin Cottrell, Wake State University, Nov 2020


## Hansl and Gretl

Gretl is a cross-platform statistical package for econometric analysis. Hansl is a recursive acronym: it stands for "Hansl's A Neat 
Scripting Language".

Start an intereactive job (change parameters as desired), source the old build system and load the gretl module. View the file 
`first_ex.inp` and execute. The interactive gretl command-line is invoked with `gretcli`.

Has symbols, #, in Hansl are comments for a single line or /* and */ for multiline comments. Newlines are implicit terminators to 
statements with the backslash (\) as a continuing line. Escape sequences (eg., "\n") are not recognised and will be interpreted 
literally. Variable assignment is marked with a datatype, name, and equals symbol for assignment. Variable identifiers are limited 
to 31 characters and can contain only only letters, numbers and the underscore character. Variables are case-sensitive and can be of 
the type scalar, series, matrix, list, string, bundle or array. The string delimiter is a double-quote, e.g., "string". Scalar 
variables have no delimiter, and there is no distinction between integers, reals, etc. All scalars are double-precision floating 
point numbers. All variables are strongly typed, and cannot be re-assigned; instead they must be deleted then re-assigned. The eval 
command is used to provide the result of an expression without assigning it to a variable.


```
$ sinteractive --ntasks=2 --partition=snowy --time=05:00:00 --x11=first
..
$ source /usr/local/module/spartan_old.sh
$ module load gretl/2018c-GCC-6.2.0-LAPACK-3.8.0-OpenBLAS-0.3.5
$ module load LAPACK/3.8.0-GCC-6.2.0-OpenBLAS-0.3.5
$ module load OpenSSL/1.0.2l-GCC-6.2.0
$ module load gnuplot/5.0.0-GCC-6.2.0
$ cp -r /usr/local/common/Gretl .
$ cd gretl
$ cat first_ex.inp
$ gretlcli -b first_ex.inp
$ gretlcli
...
```

## Scalars 

Algebraic operations have the usual rules for precedence. The caret (^) is used for exponentiation. There is a wide range of 
algebriac functions including:

abs (absolute value)
corr (correlation coefficient)
cov (covariance)
exp (exponential)
log (natural logarithm)
log10 (base 10 logarithm)
mean (returns scalar mean, skips missing observations)
median (returns median)
sd (sample standard deviation) 
sum (return sum of elements)
sqrt (square root) 

A full list is available at the following URL:

http://gretl.sourceforge.net/gretl-help/funcref.html

There are a number of built-in identifiers that are prefixed with a $-symbol (e.g., $pi). 

Whilst there is no specific Boolean type, scalars can be used for holding true/false values. Logical operators and (&&), or (||), 
and not (!) can be used. e.g.,

`a = 1 b = 0 c = !(a && b)`	# c will equal 1, as (a && b) is false.

Post-incrementation is available (b = a++), but not pre-incrementation (no b = ++a). Inflected assignments are available (e.g., a += 
b, the equivalent of a = a + b). There is no value of NaN ("not a number"); the closest equivalent is is the missing value, i.e., 
NA. One cannot test for equality (==) with NA.

## Strings

Strings can be manipulated by operators and functions. Functions (see proir reference for list) include such options as:

strlen (string length)
strncmp (compare two string arguments)
strsplit (split a string on whitespace)
strstr (search a string for a string)
strstrp (remove leading and trailing whitespace)
strsub (find and replace in a string)
substr (return a part of a string) 

The ~ operator can be used to join strings:

string s1 = "sweet"
string s2 = "Home , " ~ s1 ~ " home."

Note that strings may be arbitrarily long and can contain special characters such as line breaks and tabs. Thus many command-line 
functions can be performed in Gretl.

# Matricies

Matrices are one- or two-dimensional arrays of double-precision floating-point numbers. Matrix elements have a [r,c] syntax, where 
indexing starts at 1. For example, X[3,4] indicates the element of X on the third row, fourth column. If the only one row (column), 
the column (row) specification can be omitted, as in x[3]. Including the comma but omitting the row or column specification means 
"take them all", as in x[4,] (fourth row, all columns). For square matrices, the special syntax x[diag] can be used to access the 
diagonal. Consecutive rows or columns can be specified via the colon (:) character, as in x[,2:4] (columns 2 to 4). A vector to hold 
indices to a matrix. E.g. if e = [2, 3, 6], then X[,e] contains the second, third and sixth columns of X. Moreover, matrices can be 
empty (zero rows and columns).

A matrix is defined by rows; the elements on each row are separated by commas and rows are separated by semicolons. The whole 
expression must be wrapped in braces. Spaces within the braces are not significant.

Matrix sum, difference and product are obtained via +, - and *, respectively. The prime operator (') can act as a unary operator, in 
which case it transposes the preceding matrix, or as a binary operator, in which case it acts as in ordinary matrix algebra, 
multiplying the transpose of the first matrix into the second one.

Other matrix operators include ˆ (matrix power), ** (Kronecker product), and the concatenation operators, ~ (horizontal) and | (vertical).

Hansl also supports matrix left- and right-"division", via the \ and / operators, respectively. The expression A\b solves Ax = b for 
the unknown x.

```
matrix a = {11, 22 ; 33, 44}			# a is square 2 x 2
matrix b = {1,2,3; 3,2,1}			# b is 2 x 3
matrix c = a’
matrix d = a*b # c is the transpose of a	# d is a 2x3 matrix equal to a times b
matrix gina = b'd				# valid: gina is 3x3
matrix lina = d + b 				# valid: lina is 2x3
matrix A = {2,1;0,1}
matrix B = {1,1;1,0}
matrix KP = A ** B
matrix PWR = A^3
matrix HC = A ~ B
matrix VC = A | B
print A B KP PWR HC VC
```

There is also a wide variety of matrix functions. Most scalar functions, such as abs(), log() etc., will operate on a matrix 
element-by-element.

e.g.,
rows(X), cols(X)		return the number of rows and columns of X, respectively
zeros(r,c), ones(r,c) 		produce matrices with r rows and c columns, filled with zeros and ones, respectively
mshape(X,r,c) 			rearrange the elements of X into a matrix with r rows and c columns
seq(a,b) 			generate a row vector containing integers from a to b
inv(A)				invert, if possible, the matrix A
maxc(A), minc(A), meanc(A) 	return a row vector with the max, min, means of each column of A, respectively
maxr(A), minr(A), meanr(A) 	return a column vector with the max, min, means of each row of A, respectively
mnormal(r,c), muniform(r,c) 	generate r ×c matrices filled with standard Gaussian and uniform pseudo-random numbers, respectively

Example code:

```
$ gretlcli -b second_ex.inp
```

## Structured Data Types

Hansl possesses two kinds of "structured data type": associative arrays, called "bundles" and arrays of the same data type.

To use a bundle you first either declare it, as in `bundle foo` or define an empty bundle using the null keyword: `bundle foo = null`.

The difference is that the second variant may be reused — if a bundle named foo already exists the effect is to empty it—while the 
first may only be used once in a given gretl session; it is an error to declare a variable that already exists.

To add an object to a bundle you assign to a compound left-hand value: the name of the bundle followed by the key. The most common 
way to do this is to join the key to the bundle name with a dot, as in foo.matrix1 = m

An alternative way to achieve the same effect is to give the key as a quoted string literal enclosed in square brackets, as in 
foo["matrix1"] = m

To get an item out of a bundle, again use the name of the bundle followed by the key, as in

matrix bm = foo.matrix1
# or using the long-hand notation
matrix m = foo["matrix1"]

The key identifying an object within a given bundle is necessarily unique.

A quicker way, introduced in gretl 2017b, is to use the defbundle function, as in `bundle b = defbundle("s", "Sample string", "m", I(3))`

Note that when you add an object to a bundle, what in fact happens is that the bundle acquires a copy of the object. The external 
object retains its own identity and is unaffected if the bundled object is replaced by another.

```
bundle foo
matrix m = I(3)
foo.mykey = m
scalar x = 20
foo.mykey = x
```

To delete an object from a bundle use the delete command, with the bundle/key combination, as in

```
delete foo.mykey
delete foo["quoted key"]
```

If b is a bundle and you say print b, you get a listing of the bundle’s keys along with the types of
the corresponding objects, as in

```
? print b
bundle b:
x (scalar)
mat (matrix)
inside (bundle)
```

See example, third_ex.inp

The bundle so obtained is a container that can be used for all sort of purposes.

For example, the next code snippet illustrates how to use a bundle with the same structure as the one created above to perform an 
out-of sample forecast. Imagine that k = 4 and the value of x for which we want to forecast y is x 0 = [10 1 − 3 0.5], where CI is 
the (approximate) 95 percent confidence interval.

```
x = { 10, 1, -3, 0.5 }
scalar ypred	= x * my_model.betahat
scalar varpred	= my_model.s2 + qform(x, my_model.vcv)
scalar sepred	= sqrt(varpred)
matrix CI_95	= ypred + {-1, 1} .* (1.96*sepred)
print ypred CI_95
```

A gretl array is a container which can hold zero or more objects of a certain type, indexed by consecutive integers starting at 1. 
It is one-dimensional. This type is implemented by a quite “generic” back-end. The types of object that can be put into arrays are 
strings, matrices, bundles and lists; a given array can hold only one of these types.

```
strings S1 = array(3)
matrices M = array(4)
strings S2 = defarray("fish", "chips")
S1[1] = ":)"
S1[3] = ":("
M[2] = mnormal(2,2)
print S1
eval inv(M[2])
S = S1 + S2
print S
```

The array() takes an integer argument for the array size; the defarray() function takes a variable number of arguments (one or 
more), each of which may be the name of a variable of the given type or an expression which evaluates to an object of that type.

In order to find the number of elements in an array, you can use the nelem() function.


## Numerical Methods

We use for illustration here a classic function from the numerical optimization literature, namely the Himmelblau function, which 
has four different minima; f (x, y) = (x^2 + y − 11)^2 + (x + y^2 − 7)^2 .

```
function scalar Himmelblau(matrix x)
/* extrema:
f(3.0, 2.0) = 0.0,
f(-2.805118, 3.131312) = 0.0,
f(-3.779310, -3.283186) = 0.0
f(3.584428, -1.848126) = 0.0
*/
scalar ret = (x[1]^2 + x[2] - 11)^2
return -(ret + (x[1] + x[2]^2 - 7)^2)
end function
# ----------------------------------------------------------------------
set max_verbose 1

matrix theta1 = { 0, 0 }
y1 = BFGSmax(theta1, "Himmelblau(theta1)")
matrix theta2 = { 0, -1 }
y2 = NRmax(theta2, "Himmelblau(theta2)")
print y1 y2 theta1 theta2
```

For numerical differentiation we have fdjac.

```
set echo off
set messages off

function scalar beta(scalar x, scalar a, scalar b)
	return x^(a-1) * (1-x)^(b-1)
end function

function scalar ad_beta(scalar x, scalar a, scalar b)
	scalar g = beta(x, a-1, b-1)
	f1 = (a-1) * (1-x)
	f2 = (b-1) * x
	return (f1 - f2) * g
end function

function scalar nd_beta(scalar x, scalar a, scalar b)
	matrix mx = {x}
	return fdjac(mx, beta(mx, a, b))
end function

a = 3.5
b = 2.5

loop for (x=0; x<=1; x+=0.1)
	printf "x = %3.1f; beta(x) = %7.5f, ", x, beta(x, a, b)
	A = ad_beta(x, a, b)
	N = nd_beta(x, a, b)
	printf "analytical der. = %8.5f, numerical der. = %8.5f\n", A, N
endloop
```

## Control Flow

The primary means for controlling the flow of execution in a hansl script are the if statement (conditional execution), the loop 
statement (repeated execution), the catch modifier (which enables the trapping of errors that would otherwise halt execution), and 
the quit command (which forces termination).

Conditional execution in hansl uses the if keyword. Its fullest usage is as follows
if <condition>
...
elif <condition>
...
else
...
endif

The <condition> can be any expression that evaluates to a scalar: 0 is interpreted as “false”,
non-zero is interpreted as “true”; NA generates an error.
• Following if, “then” is implicit; there is no then keyword as found in, e.g., Pascal or Basic.
• The elif and else clauses are optional: the minimal form is just if . . . endif.
• Conditional blocks of this sort can be nested up to a maximum of depth of 1024.

The ternary query operator

Besides use of if, the ternary query operator, ?:, can be used to perform conditional assignment. This has the form

result = <condition> ? <true-value> : <false-value>

If <condition> evaluates as true (non-zero) then <true-value> is assigned to result, otherwise result will contain <false-value>. 1 
This is obviously more compact than if . . . else . . . endif. 

The following example replicates the abs function by hand: 

`scalar ax = x>=0 ? x : -x`

Which is the equivalent of `ax = abs(x)`

Consider, however, the following case, which exploits the fact that the ternary operator can be nested: 

`scalar days = (m==2) ? 28 : maxr(m.={4,6,9,11}) ? 30 : 31`


The basic hansl command for looping is loop, and takes the form

loop <control-expression> <options>
...
endloop

In other words, the pair of statements loop and endloop enclose the statements to repeat. Of course, loops can be nested. Several 
variants of the <control-expression> for a loop are supported, as follows:

1. unconditional loop
2. while loop
3. index loop
4. foreach loop
5. for loop


The unconditional loop is used quite rarely, as in most cases it is useful to have a counter variable (count in the previous 
example). This is easily accomplished via the index loop, whose syntax is loop <counter>=<min>..<max> ... endloop

The while loop has

loop while <condition>
...
endloop

loop foreach <counter> <catalogue>
...
endloop

... where <catalogue> can be either a collection of space-separated strings, or a variable of type list

The final form of loop control emulates the for statement in the C programming language. The syntax is loop for, followed by three 
component expressions, separated by semicolons and surrounded by parentheses, that is 

loop for (<init>; <cont>; <modifier>) 
	... 
endloop


