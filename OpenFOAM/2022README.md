# Part I: Preprocess 1 core OpenFOAM Task

This is the most simple process, based on the Lid-driven cavity flow from the OpenFOAM examples 
(`https://www.openfoam.com/documentation/tutorial-guide/tutorialse2.php#x6-70002.1.1`)

The first example simply copies the cavity example and, from with the blockMesh command from the parameters in `blockMeshDict` in 
`cavity/cavity/system`.

# Viewing the Mesh

This requires an interactive job.

1. Login with X-windows forwarding, launch the job

ssh username -X
sbatch 2022openfoam-single-1.slurm

When the job completes check the output e.g.,

less slurm-14658.out 

2. Launch an interactive job with X-windows forwarding

sinteractive --x11=first --time=1:00:00

3. Load the module and source the application parameters.

module purge
module load foss/2022a
module load OpenFOAM/v2206
source $FOAM_BASH
cd cavity/cavity
ParaView/5.10.1-mpi

4. Launch Paraview

paraFoam

Select 'Apply' under properties which will launch the Mesh
After that you can (for example) change the Display to use the Representation from 'Surface' to 'Wireframe'.

# Part II Process 1 Core OpenFOAM Task

1. Login with X-windows forwarding, launch the job

ssh username -X
sbatch openfoam-single-2.slurm

When the job completes check the output	e.g.,

less slurm-7655290.out

2. Launch an interactive job with X-windows forwarding

sinteractive --x11=first --time=1:00:00

3. Load the module and source the application parameters.

module purge
module load foss/2019b
module load openfoam/7
source $FOAM_BASH
cd cavity/cavity

4. Launch Paraview

paraFoam
`Select` Apply under properties which will launch the Mesh

Select the Properties panel for the cavity.OpenFOAM case module. 

After selecting 'Apply', chosing under 'Coloring', 'p', and 'Rescale to Data Range' (button next to 'Edit').

Run VCR Controls (play button, top menu)

Change Preset for Blue-Red colour rainbow if desired. 

# OpenFOAM and RapidCFD

Please note that this tutorial is NOT YET COMPLETE

OpenFOAM can use RapidCFD for GPU acceleration. With RapidCFD, you have to specify the devices you want to use.

For example you set `--gres=gpu:1, and --nodes=4`, need to do

`srun -N 4 pulsatilePipe -parallel -devices "(0 0 0 0)" > log0`

The 0's after the devices option are the indices for the GPUs. Each node has 4 GPUs, and the indices range from 0-3 for the 4 GPUs. 0 is the 
1st GPU on the node, 1 the second etc etc. So you are asking for the 1st GPU on each node.
