#!/usr/bin/env perl

use strict;
use warnings;

use BerkeleyDB;

# Open the database
my $filename = 'example.bdb';
my $db = new BerkeleyDB::Hash (
-Filename => $filename,
-Flags => DB_CREATE,
);

# Add some data
$db->db_put('key1', 'value1');
$db->db_put('key2', 'value2');
$db->db_put('key3', 'value3');

# Retrieve data and write to file
my $value;
$db->db_get('key1', $value, undef);
open(my $fh, '>', 'output.txt') or die "Could not open file 'output.txt' $!";
print $fh "The value of key1 is $value\n";
close $fh;

# Close the database
undef $db;
unlink $filename;

