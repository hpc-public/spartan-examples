# MariaDB on Spartan with an Interactive Session

## 1. Setting up the Configuration

First create a configuration file, my.cnf like the one below and modified for particular usernames and project directories.

$ cat ~/.my.cnf 
[mysqld]
datadir=/data/projects/hpcadmin/lev/mariadb
socket=/data/projects/hpcadmin/lev/mariadb/mariadb.sock
user=lev
symbolic-links=0
skip-networking
$ module load foss/2019b
[mysqld_safe]
log-error=/data/projects/hpcadmin/lev/mariadb/mariadbd.log
pid-file=/data/projects/hpcadmin/lev/mariadb/mariadbd.pid

[mysql]
socket=/home/lev/hpcadmin/lev/mariadb/mariadb.sock


## 2. Set up and start the MariaDB session. 

Start an interactive job, load the modules, initialise the database server, and start the server in safe mode.


$ sinteractive --time=1:00:00
$ module load foss/2019b
$ module load mariadb/10.3.14
$ /usr/local/easybuild-2019/easybuild/software/mpi/gcc/8.3.0/openmpi/3.1.4/mariadb/10.3.14/scripts/mysql_install_db
$ mysqld_safe
230418 14:07:03 mysqld_safe Logging to '/data/projects/hpcadmin/lev/mariadb/mariadbd.log'.
230418 14:07:03 mysqld_safe Starting mysqld daemon with databases from /data/projects/hpcadmin/lev/mariadb

## 3. Run an interactive session

SSH into the compute node that the server is running out. load modules, and start a client session.

$ ssh spartan-bm083
Last login: Tue Apr 18 12:23:30 2023 from spartan-login3.hpc.unimelb.edu.au
$ module load foss/2019b
$ module load mariadb/10.3.14
$ mysql
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.3.14-MariaDB [test]> DESCRIBE Information;
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| firstname | varchar(20) | YES  |     | NULL    |       |
| lastname  | varchar(20) | YES  |     | NULL    |       |
| gender    | char(1)     | YES  |     | NULL    |       |
| grade     | int(10)     | YES  |     | NULL    |       |
| dob       | date        | YES  |     | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
5 rows in set (0.001 sec)
MariaDB Source distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> 

## 4. Enter the interactive database commands; exit, cancel session

MariaDB [(none)]> use test
MariaDB [(none)]> CREATE TABLE Information (firstname VARCHAR(20),lastname VARCHAR(20),gender CHAR(1),grade INT(10), dob DATE);
MariaDB [test]> exit
Bye
$ showq -u lev

SUMMARY OF JOBS FOR USER: <lev>

ACTIVE JOBS--------------------
JOBID     JOBNAME    USERNAME      STATE     CORE   REMAINING  STARTTIME
==================================================================================
46509537  interactiv lev           Running   1        0:12:23  Tue Apr 18 13:59:38

$ scancel 46509537
