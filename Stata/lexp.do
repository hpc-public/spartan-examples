* Example do file
* Life Exptectancy and GNPPC
set more off
capture log close
sysuse lifeexp, clear
desc
summarize lexp gnppc
list country gnppc if missing(gnppc)
graph twoway scatter lexp gnppc
graph export scatter.png, width(550) replace
regress lexp loggnppc
graph twoway (scatter lexp loggnppc) (lfit lexp loggnppc)
log close

