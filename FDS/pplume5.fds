!!! General configuration

&HEAD CHID='pplume5', TITLE='Plume case' /
      name of the case and a brief explanation

&TIME T_END=10.0 /
      the simulation will end at 10 seconds

&MISC SURF_DEFAULT='wall', TMPA=25. /
      all bounding surfaces have
      a 'wall' boundary condition
      unless otherwise specified,
      the ambient temperature is set to 25°C.

&REAC ID='polyurethane', SOOT_YIELD=0.10,
      N=1.0, C=6.3, H=7.1, O=2.1 /
      predominant fuel gas for the mixture fraction model
      of gas phase combustion

!!! Computational domain

&MESH IJK=32,32,16, XB=0.0,1.6,0.0,1.6,0.0,0.8 /
&MESH IJK=32,32,16, XB=0.0,1.6,0.0,1.6,0.8,1.6 /
&MESH IJK=32,32,16, XB=0.0,1.6,0.0,1.6,1.6,2.4 /
&MESH IJK=32,32,16, XB=0.0,1.6,0.0,1.6,2.4,3.2 /
      four connected calculation meshes
      and their cell numbers

!!! Properties

&MATL ID='gypsum_plaster', CONDUCTIVITY=0.48,
      SPECIFIC_HEAT=0.84, DENSITY=1440. /
      thermophysical properties of 'gypsum plaster' material

&PART ID='tracers', MASSLESS=.TRUE., SAMPLING_FACTOR=1 /
      a type of Lagrangian particles

&SURF ID='burner', HRRPUA=600.,
      PART_ID='tracers', COLOR='RASPBERRY' /
      a type of boundary conditions named 'burner'

&SURF ID='wall', RGB=200,200,200, MATL_ID='gypsum_plaster',
      THICKNESS=0.012 /
      a type of boundary conditions named 'wall'

!!! Solid geometry

&VENT XB=0.5,1.1,0.5,1.1,0.1,0.1, SURF_ID='burner' /
      the 'burner' boundary condition
      is imposed to a plane face

&OBST XB=0.5,1.1,0.5,1.1,0.0,0.1, SURF_ID='wall' /
      a solid is created, 'wall' boundary condition
      is imposed to all its faces

&VENT XB=0.0,0.0,0.0,1.6,0.0,3.2, SURF_ID='OPEN'/
&VENT XB=1.6,1.6,0.0,1.6,0.0,3.2, SURF_ID='OPEN'/
&VENT XB=0.0,1.6,0.0,0.0,0.0,3.2, SURF_ID='OPEN'/
&VENT XB=0.0,1.6,1.6,1.6,0.0,3.2, SURF_ID='OPEN'/
&VENT XB=0.0,1.6,0.0,1.6,3.2,3.2, SURF_ID='OPEN'/
      the 'OPEN' boundary condition is imposed to
      the exterior boundaries of the computational domain

!!! Output

&DEVC XYZ=1.2,1.2,2.9, QUANTITY='THERMOCOUPLE', ID='tc1' /
      send to output: the data collected by a thermocouple
&ISOF QUANTITY='TEMPERATURE', VALUE(1)=100.0 /
      3D contours of temperature at 100°C
&SLCF PBX=0.8, QUANTITY='TEMPERATURE', VECTOR=.TRUE. /
      vector slices colored by temperature
&BNDF QUANTITY='WALL TEMPERATURE' /
      surface 'WALL_TEMPERATURE' at all solid obstructions
&TAIL / end of file
