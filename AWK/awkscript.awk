BEGIN {
FS=","
print " Earthquake Locations"
print " Date                      Latitude       Longitude"
print "---------------------------------------------------"
}

(NR > 1) { 
print $2 "  \t  " $3 " \t " $4;

}

END {
print "Earthquake Data New Zealand, 2003"
}
