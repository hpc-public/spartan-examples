Content derived from the Swedish University of Agricultural Sciences

https://www.hadriengourle.com/tutorials/

QUAST is a software evaluating the quality of genome assemblies by computing various metrics.

The file  `m_genitalium.fasta` is a copy of `m_genitalium.fasta` which is produced by the de novo assembly in the MEGAHIT example.

The quality report will be in m_genitalium_report in various formats.
